import ballerina/kafka;
 
kafka:ProducerConfig producerConfigs = {
  
    bootstrapServers: "localhost:9092",
    clientId:"basic-producer",
    acks:"all",
    noRetries:3
};
 
kafka:Producer kafkaProducer = new(producerConfigs);
 
function main () {
    string msg = "DSA Assignment 2";
    byte[] serializedMsg = msg.toByteArray("UTF-8");
    var sendResult = kafkaProducer-&gt;send(serializedMsg, "topic1");
    if (sendResult is error) {
        log:printError("Kafka producer failed to send data", err = sendResult);
    }
}
 