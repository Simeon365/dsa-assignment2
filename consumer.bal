import ballerina/kafka;
import ballerina/encoding;
import ballerina/io;
 
kafka:ConsumerConfig consumerConfigs = {
    bootstrapServers:"localhost:9092",
    groupId:"group-id",
    topics:["topic1"],
    pollingInterval:1000
};
 
listener kafka:Consumer consumer = new(consumerConfigs);
 
service kafkaService on consumer {
 
    resource function onMessage(kafka:ConsumerAction consumerAction, kafka:ConsumerRecord[] records) {
        foreach var kafkaRecord in records {
            processKafkaRecord(kafkaRecord);
        }
    }
}
 
function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) {
    byte[] serializedMsg = kafkaRecord.value;
    string msg = encoding:byteArrayToString(serializedMsg);
   
    io:println("Topic: " + kafkaRecord.topic + "  Message: " + msg);
}